%
% File acl2015.tex
%
% Contact: car@ir.hit.edu.cn, gdzhou@suda.edu.cn
%%
%% Based on the style files for ACL-2014, which were, in turn,
%% Based on the style files for ACL-2013, which were, in turn,
%% Based on the style files for ACL-2012, which were, in turn,
%% based on the style files for ACL-2011, which were, in turn, 
%% based on the style files for ACL-2010, which were, in turn, 
%% based on the style files for ACL-IJCNLP-2009, which were, in turn,
%% based on the style files for EACL-2009 and IJCNLP-2008...

%% Based on the style files for EACL 2006 by 
%%e.agirre@ehu.es or Sergi.Balari@uab.es
%% and that of ACL 08 by Joakim Nivre and Noah Smith

\documentclass[11pt]{article}
\usepackage{acl2015}
\usepackage{times}
\usepackage{url}
\usepackage{latexsym}
\usepackage{microtype}
\usepackage{amsmath}
\usepackage{algorithmic}
\usepackage{lineno}
\usepackage[ruled,vlined]{algorithm2e}
\usepackage{amssymb}
\usepackage{booktabs}
\usepackage{pbox}
\usepackage{multicol}

%\setlength\titlebox{5cm}

% You can expand the titlebox if you need extra space
% to show all the authors. Please do not make the titlebox
% smaller than 5cm (the original size); we will check this
% in the camera-ready version and ask you to change it back.


\title{Do We Need Simplified Corpora? \\ Lexical Simplification Using Complex Corpora Only}

\author{First Author \\
  Affiliation / Address line 1 \\
  Affiliation / Address line 2 \\
  Affiliation / Address line 3 \\
  {\tt email@domain} \\\And
  Second Author \\
  Affiliation / Address line 1 \\
  Affiliation / Address line 2 \\
  Affiliation / Address line 3 \\
  {\tt email@domain} \\}

\date{}

\begin{document}
\maketitle
\begin{abstract}

Simplification of lexically complex texts, by replacing complex words with their simpler synonyms, helps various people such as non-native speakers, children, and language-impaired people understand the text. Recent systems for lexical simplification typically involve data-driven approaches on parallel or comparable corpora of standard-language and manually simplified texts, which is expensive and time-consuming to build. We present an unsupervised method for lexical simplification based on recent breakthroughs in word vector representations that does not require having simplified corpora. Results of both human and automated evaluation show that the proposed approach is comparably effective to existing simplification systems relying on manually simplified corpora.        

\end{abstract}

\section{Introduction}

% 3. paragraf: diff -- naglasak na resursnu zahtjevnost postojećih metoda (simplified corpora, sentence-alignment, wordnet, ...) i kako mi ne trebamo ništa od toga. High-level opis embeddings-based metode

% 1. paragraf: motivacija za (leksičku) simplifikaciju

Lexical complexity is often the cause of difficulties in text understanding for various groups of people: non-native speakers \cite{Petersen-07}, children \cite{DeBelder-10-short}, people with intellectual disabilities \cite{Feng-09}, and language-impaired people such as autistic \cite{Martos-12}, aphasic \cite{Carroll-98}, or dyslexic \cite{Rello-12}. Automated text simplification by means of replacing complex words with their more common or simpler synonyms is needed to help these people grasp the information expressed by the text more easily.     

% 2. paragraf: high-level view of the related work (od prve ideje frekvencijske zamjene riječi do Birana i Kavuchuka)

Systems for lexical simplification are still dominantly rule-based, with a set of rules for substituting long and infrequent words with their shorter and more frequent synonyms \cite{Devlin-98,Burstein-07-short,DeBelder-10-short}. In order to generate the substitution rules, most of these systems rely on lexico-semantic resources such as WordNet for finding synonyms of a complex word. The fact that lexicons like WordNet exist for very few languages limits the impact of such simplification methods.  

The emergence of the Simple English Wikipedia moved the focus to data-driven approaches \cite{Yatskar-10-short,Biran&al'11,horn2014learning} where unsupervised approaches leveraged either the metadata (e.g., edit history) \cite{Yatskar-10-short} or co-occurrence statistics of the simplified corpora \cite{Biran&al'11}, whereas the supervised methods learn substitutions from the sentence-aligned subset of the comparable corpora \cite{horn2014learning} in order to generate substitution rules. Although usage of the simplified corpora improves the simplification performance, it also effectively limits the applicability of these methods to very few languages for which such corpora exists. 

The research question we address in this work is whether a solid performance in lexical simplification can be achieved without resourcing to simplified corpora or lexicons such as WordNet. Following the the observation that ``simple'' words are also abundant in standard-language text, we leverage recent results in word vector representations \cite{mikolov2013distributed,pennington2014glove} to find suitable simplifications for complex words. We thoroughly evaluate the performance of the proposed approach (1) on the lexical substitution task \cite{horn2014learning}, (2) on the SemEval-2012 English Lexical Simplification task \cite{specia2012semeval}, and (3) via human judgements of grammaticality, simplicity, and meaning preservation. Obtained results support our hypothesis that comparable simplification performance can be achieved without simplified corpora and other resources.          

\section{Related Work}

% Early approaches

Traditional systems for lexical simplification for English are dominantly rule-based, i.e., they rely on a set or rules for substituting long and infrequent words with their shorter and more frequent synonyms. Creating the substitution rules amounts to identifying synonyms, usually in WordNet, for a predefined set of complex words \cite{Carroll-98,Bautista-09-short}, and then choosing the ``simplest'' of these synonyms, typically with some frequency-based \cite{Devlin-98,DeBelder-10-short} or length-based heuristics \cite{Bautista-09-short}. The main shortcomings of the rule-based systems include low recall \cite{DeBelder-10-short} and misclassification of simple words as complex (and vice versa) \cite{Shardlow-14-long}.

% Pojava simple wikipedije i data-driven pristupa
 
The shift from knowledge-based lexical simplification paradigm to the data-driven one came with the  creation of Simple English Wikipedia \cite{}, which together with the `original' English Wikipedia offered a large comparable corpus of original and simplified texts, shifted the focus of lexical simplification to data-driven approaches. Yatskar {\it et al.} \shortcite{Yatskar-10-short} used edit histories in Simple English Wikipedia to extract lexical simplifications. They employed a probabilistic model to discern simplification edits from other types of content edits. 

Biran {\it et al.} \shortcite{Biran&al'11} presented an unsupervised method for learning substitution pairs from a corpus of comparable texts from Wikipedia and Simple English Wikipedia. Unlike Yatskar {\it et al.} \shortcite{Yatskar-10-short}, Biran {\it et al.} \shortcite{Biran&al'11} exploit the content of the simplified corpus in terms of (co-)occurrence statistics rather than its metadata. 

Horn {\it et al.} \shortcite{horn2014learning} proposed a supervised model for learning the simplification rules. They generate the candidate simplification rules using a sentence-aligned corpora consisting of 137K sentence pairs. A binary classifier, deciding whether a candidate simplification should be applied is then trained on a set of 500 Wikipedia sentences, manually annotated via crowdsourcing.

% Pokuda potrebe za resursima

The main limitation of all of the aforementioned approaches is the dependence on either or simplified corpora or some knowledge source (e.g., WordNet or Wikipedia metadata). In contrast, we propose a resource-light methodology for lexical simplification requiring nothing but a sufficiently large corpus of complex text, making it easily applicable to very many languages lacking such resources. 

\section{Resource-Light Lexical Simplification}

% Uvodno: sažeti opis cijelog pristupa, spomenuti svaki od koraka
Our approach is based on the intuition that ``simple'' words, besides appearing frequently in simple corpora, also appear sufficiently frequently in regular (i.e.,~``complex'') texts, which means that we can find simple synonyms of complex words in complex corpora as well, provided reliable methods for measuring (1) the ``complexity'' of the word and (2) the semantic similarity of words. In this work, we associate word complexity with the commonness of the word in the corpus, and not with the length of the word.  

\subsection{Candidate Selection}
\label{sec:candidateselection}

We employ GloVe \cite{pennington2014glove}, a state-of-the-art model of distributional lexical semantics in order to obtain vector representations for all corpus words. The semantic similarity of two words is computed as the cosine of the angle their corresponding GloVe vectors. For each content word (noun, verb, adjective, or adverb) $w$, we select as simplification candidates the top $n$ words whose GloVe vectors are most similar to that of word $w$.

In all our experiments we used the 200-dimensional GloVe vectors pretrained on the six billion tokens corpus which is a merge of the entire English Wikipedia (dump from January 2014) and the Gigaword 5 corpus\footnote{\url{http://www-nlp.stanford.edu/data/glove.6B.200d.txt.gz}}. For each content word, we select $n=10$ most similar words, excluding the morphological derivations of the original word.  

\subsection{Goodness-of-Simplification Features}
\label{sec:features}

In the next step, we rank the simplification candidates according to several criteria (i.e., features). Each of the features captures one aspect of the suitability of the candidate word to replace the original word. The following are the descriptions of individual features. 

\paragraph{Semantic similarity.} This feature is computed simply as the cosine of the angle between the GloVe vector of the original word and the GloVe vector of the simplification candidate. 

\paragraph{Context similarity.} Since distributional lexico-semantic models cannot discriminate between senses of polysemous words, considering only semantic similarity between original and candidate word may lead to semantically incompatible simplifications for polysemous content words in the original text. We hypothesize that the simplification candidates being synonyms of the correct sense of the polysemous original word will be more similar with the context of that word. We compute this feature by averaging the semantic similarities between the simplification candidate and each content word in the context of the original content word:
%
\begin{equation*}
\mathit{csim}(w, c) = \frac{1}{|C(w)|}\sum_{w' \in C(w)}{\hspace{-0.6em}\cos(\mathbf{v_{w'}}, \mathbf{v_{w'}})}
\end{equation*}    
%
where $C(w)$ is the set of context words of the original word $w$ and $\mathbf{v_{w}}$ is the GloVe distributional vector of the word $w$. In all our experiments we set the context of the original word to be the symmetric window of size three around that word.   

\paragraph{Difference of information contents.} This feature's primary purpose is to determine whether the simplification candidate word is more informative than the original content word. Under the hypothesis that word's informativeness is correlated with its complexity, we would like to enforce simplification candidate to be less informative than the original word. The complexity of the word is estimated by computing the information content of the word as follows: 
%
\begin{equation*}
\mathit{ic(w)} = -\log{\frac{\mathit{freq}(w) + 1}{\sum_{w' \in C}{\mathit{freq}(w') + 1}}}
\end{equation*}
%
where $\mathit{freq(w)}$ is the frequency of the word $w$ in a large corpus $C$. We used word frequencies from the Google Book Ngrams corpus \cite{michel2011quantitative} for the computation of information contents. The final value of this feature is the difference between the information content of the original word and the information content of the simplification candidate word, aiming to approximate the reduction (or gain, if the difference is negative) in complexity that would be introduced should the simplification candidate replace the original word. 

\paragraph{Language model fitness.}    

The intuition for having the language model features is rather obvious -- a simplification candidate is a compatible substitute for the original word if it fits into the sequence of words preceding and following the original word. Let $w_{-2} w_{-1} w w_1 w_2$ be the sequence of words in the original text with $w$ being the word we are considering simplifying. We consider a simplification candidate $c$ to be a compatible replacement for $w$ if the sequence $w_{-2} w_{-1} c w_1 w_2$ has been observed often in a large corpus, i.e., if the language model would assign a relatively high probability to that sequence. We used the Berkeley language model \cite{pauls2011faster} to compute language model features. Since Berkeley language model contains only bigrams and trigrams, we obtained the following language model scores for each simplification candidate $c$: $w_{-1} c$, $cw_{1}$, $w_{-2} w_{-1} c$, $c w_{1} w_{2}$, and $w_{-1} c w_{1}$.            

\subsection{Simplification Algorithm}

The overall simplification algorithm is given in Algorithm \ref{alg:simplificationsentence}. 
%
{\linespread{1}
\begin{algorithm}
\caption{Simplify($tt$)}
\label{alg:simplificationsentence}
\begin{algorithmic}[1]
\STATE \textbf{Input}: tokenized text $\mathit{tt}$
\STATE \textbf{Simplify}:
\vspace{0.3em}
\STATE \hspace{1em} $\mathit{subst} \gets \varnothing$
\STATE \hspace{1em} \textbf{for each} content token $t \in \mathit{tt}$ \textbf{do} 
\STATE \hspace{2em} $\mathit{all\_ranks} \gets \varnothing$
\STATE \hspace{2em} $\mathit{scs} \gets \mathit{most\_similar}(t)$
\STATE \hspace{2em} \textbf{for each} feature $\mathit{f}$ \textbf{do}
\STATE \hspace{2em} $\mathit{scores}\gets \varnothing$
\STATE \hspace{3em} \textbf{for each} $\mathit{sc} \in \mathit{scs}$ \textbf{do}
\STATE \hspace{4em} $\mathit{scores} \gets \mathit{scores} \cup f(sc)$
\STATE \hspace{3em} $\mathit{rank} \gets \mathit{rank\_numbers}(\mathit{scores})$
\STATE \hspace{3em} $\mathit{all\_ranks} \gets \mathit{all\_ranks} \cup \mathit{rank}$
\STATE \hspace{2em} $\mathit{avg\_rank} \gets \mathit{average}(\mathit{all\_ranks})$
\STATE \hspace{2em} $\mathit{best} \gets \mathit{argmax}_{\mathit{sc}}(\mathit{avg\_rank})$
\STATE \hspace{2em} \textbf{if} $\mathit{ic}(\mathit{best}) < \mathit{ic}(\mathit{tt})$ \textbf{do}
\STATE \hspace{3em} $\mathit{bpos} \gets \mathit{in\_pos}(\mathit{best}, \mathit{pos}(\mathit{tt}))$
\STATE \hspace{3em} $\mathit{subst} \gets \mathit{subst} \cup (\mathit{tt}, bpos)$
\STATE \textbf{Output}: list of all simplifications $\mathit{subst}$   		
\end{algorithmic}
\end{algorithm} 
}
%
We first retrieve the list of simplification candidates for each content word (line 6 in Algorithm 1), as described in Section \ref{sec:candidateselection}. We then, compute each of the features presented in Section \ref{sec:features} for each of simplification candidates (lines 7--10) and rank the candidates according to assigned feature scores (line 11). We select as best simplification candidate the one with the highest average rank over all features (line 14) and carry out the simplification only if the best candidate has lower information content than the original content word (lines 15--17). Since simplification candidates need not have the same POS-tag as the original word, in order to preserve grammaticality we transform the chosen candidate into the morphological form that matches the POS-tag of the original word (line 16). We use the NodeBox Linguistics tool for these morphological transformations.\footnote{https://www.nodebox.net/code/index.php/Linguistics}. 

\section{Evaluation}

We evaluated the effectiveness of our simplification method automatically on two different lexical simplification tasks, but we also let humans judge the quality of the simplified text. 

\subsection{Replacement Task}

We first evaluate our simplification algorithm on the dataset compiled by \newcite{horn2014learning}. This dataset consists of 500 sentences in which authors crowdsourced 50 candidate simplifications for one target content word. The performance of the model is evaluated with three different measures: (1) \textit{precision} is the percentage of the words that system decided to simplify for which the simplification proposed by the system matches any of the human simplifications; (2) \textit{changed} is the percentage of the sentences in which the system decided to simplify the target word; and (3) \textit{accuracy} is the percentage of all words that were either not simplified or the system simplification was not found in the list of manual simplifications. We refer the reader to \newcite{horn2014learning} for more details on the evaluation metrics.       

The performance of our simplification algorithm (\textsc{Light-LS}) on the dataset is shown in Table \ref{tbl:replacementtask} along with the performance of the supervised system by \newcite{horn2014learning} and the unsupervised system by \newcite{Biran&al'11}, both utilizing simplified corpora.
%
\begin{table}
\caption{Performance on the replacement task}
\label{tbl:replacementtask}
\begin{center}
{\small
\begin{tabular}{l c c c}
\toprule
Model & Precision & Accuracy & Changed \\ \midrule
\newcite{Biran&al'11} & 71.4 & 3.4 & 5.2 \\ 
\newcite{horn2014learning} & \textbf{76.1} & 66.3 & 86.3 \\ \midrule
\textsc{Light-LS} & 71.0 & \textbf{68.2} & \textbf{96.0} \\ 
\bottomrule
\end{tabular}
}
\end{center}
\end{table}
%  
The results reveal that our resource light unsupervised approach achieves performance comparable to that of the supervised system by \newcite{horn2014learning} which relies on Simple Wikipedia. The unsupervised system by \newcite{Biran&al'11} reaches precision comparable to that of our system but at the cost of changing only about 5\% of words which results in very low accuracy. Our method numerically outperforms the supervised method of \newcite{horn2014learning}, but the difference is not statistically significant.      

\subsection{Ranking Task}

The second automated evaluation we performed was on the dataset from SemEval 2012 Task 1 on lexical simplification for English \cite{specia2012semeval}. The task consisted of ranking a target word (in a context) and three candidate replacements (offered by human annotators) by simplicity, from the simplest to the most complex. 

To account for the peculiarity of the task where the target word is at the same time one of the simplification candidates, we applied two modifications to our features from Section \ref{sec:features}: (1) we excluded the \textit{semantic similarity} feature, as it would give unfair advantage to the target word in the ranking, and (2) instead of considering the difference in information contents between the target word and the candidate we used only the information content of the simplification candidate. 

Systems participating in the task were evaluated by computing Cohen's kappa index on the concurrence upon the order of pairwise ranks. Besides the random baseline, the official evaluation compared the systems with the baseline model which ranked candidates by their frequency in a large corpus in decreasing order (the most frequent word ranks first). Our method is compared with the baselines and the best-performing system \cite{jauhar2012uow} (a supervised model) from the SemEval task in Table \ref{tbl:semevaltask}. 
%
\begin{table}
\caption{Performance on the SemEval 2012 Task 1}
\label{tbl:semevaltask}
\begin{center}
{\small
\begin{tabular}{l c}
\toprule
Model & Cohen's $\kappa$ \\ \midrule 
baseline-random & 0.013 \\ 
baseline-frequency & 0.471 \\ \midrule
\newcite{jauhar2012uow} & 0.496 \\
\textsc{Light-LS} & \textbf{0.540} \\ 
\bottomrule
\end{tabular}
}
\end{center}
\end{table}
%
Our unsupervised resource-light simplification algorithm significantly outperforms the supervised model by \newcite{jauhar2012uow} with $p < 0.05$ according to the non-parametric stratified shuffling test \cite{yeh2000more}. An interesting observation is that the competitive frequency-based baseline highly correlates with our information content-based feature (the higher the frequency, the lower the information content).

\subsection{Human Evaluation}

\section{Conclusion}

\bibliographystyle{acl}
\bibliography{references}


\end{document}
